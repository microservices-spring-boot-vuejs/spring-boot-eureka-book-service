package com.example.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@ControllerAdvice
public class GlobalExceptionAdvice {

    public GlobalExceptionAdvice() {
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NotFoundException.class)
    String NotFoundExceptionAdvice(NotFoundException ex) {
        return ex.getMessage();
    }
}

package com.example.services;

import com.example.exception.NotFoundException;
import com.example.model.Book;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.nio.charset.Charset;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class BookService {

    private List<Book> bookList = new ArrayList<>(Arrays.asList(new Book(1, "isbn", "author", "title", 399f)));

    private final AtomicInteger counter = new AtomicInteger(3);

    public BookService() {
        bookList.add(new Book(2, "978-974-489-125-9", "ไม่รู้", "One Piece", 355.25f));
    }

    public List<Book> getAll() {
        return bookList;
    }

    public Book getById(int id) {
        return bookList.stream()
                .filter(result -> result.getId() == id)
                .findFirst().orElseThrow(() -> new NotFoundException("not found book id " + id));
    }

    public void addBook(Book book) {
        bookList.add(new Book(counter.getAndIncrement(), book.getIsbn(), book.getAuthor(), book.getTitle(), book.getPrice()));
    }


    public void updateBook(int id, Book body) {
        Book book = bookList.stream()
                .filter(result -> result.getId() == id)
                .findFirst().orElseGet(() -> {
                    throw new NotFoundException("not found book id " + id);
                });
        book.setTitle(body.getTitle());
        book.setAuthor(body.getAuthor());
    }

    public void deleteBook(int id) {
        Book book = bookList.stream()
                .filter(result -> result.getId() == id)
                .findFirst().orElseGet(() -> {
                    throw new NotFoundException("not found book id " + id);
                });
        bookList.remove(book);
    }

    public List<Book> searchBookByTitle(String title) {
        return bookList.stream().filter((result) -> {
            return result.getTitle().contains(title);
        }).collect(Collectors.toList());
    }
}

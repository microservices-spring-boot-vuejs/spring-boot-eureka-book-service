package com.example.controllers;

import com.example.model.Book;
import com.example.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
public class HomeController {

    @Autowired
    private Environment env;

    @Autowired
    private BookService bookService;

    @GetMapping("/")
    public String Home() {
        return "WELCOME TO BOOK SERVICE running at port: " + env.getProperty("server.port");
    }

    @GetMapping("/books")
    public List<Book> getAllBook() {
        return bookService.getAll();
    }

    @GetMapping("books/search")
    public List<Book> searchBookByTitle(@RequestParam(required = false) String title) {
        return bookService.searchBookByTitle(title);
    }

    @GetMapping("/books/{id}")
    public Book getBookById(@PathVariable int id) {
        return bookService.getById(id);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/book")
    public String addBook(@RequestBody Book book) {
        bookService.addBook(book);
        return "add book success";
    }

    @PutMapping("/book/{id}")
    public String editBook(@PathVariable int id, @RequestBody Book book) {
        bookService.updateBook(id, book);
        return "update book success";
    }

    @DeleteMapping("/book/{id}")
    public String deleteBook(@PathVariable int id) {
        bookService.deleteBook(id);
        return "delete book success";
    }

}
